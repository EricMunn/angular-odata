import * as odata from "./../../src/angular-odata";
import { MockUUID } from "./../mocks/MockUUID";

import * as angular from 'angular';

angular.module('AngularODataTestModule', [])
    .service("odata3BatchService",odata.OData3BatchService)
    .service("uuid",MockUUID);

describe("Service: OData3BatchService", () => {
    var service:odata.IODataBatchService;
    var $q:ng.IQService;
    var uuid:MockUUID;
    var $scope:ng.IScope;
    var $httpBackend:ng.IHttpBackendService;
    var $httpParamSerializer:ng.IHttpParamSerializer;
    var $http:ng.IHttpService;
    var getRequestHandler:angular.mock.IRequestHandler;
    var confirmResolve:jasmine.Spy;
    var confirmReject:jasmine.Spy;

    beforeEach(function(){
        angular.mock.module('AngularODataTestModule');

        inject(function($rootScope, _$q_, _$httpBackend_:ng.IHttpBackendService, _$httpParamSerializer_:ng.IHttpParamSerializer,
            odata3BatchService:odata.IODataBatchService, _uuid_:MockUUID){
            service = odata3BatchService;
            $q = _$q_;
            $scope = $rootScope.$new();
            $httpBackend = _$httpBackend_;
            $httpParamSerializer = _$httpParamSerializer_;
            uuid = _uuid_;
        });

        confirmResolve = jasmine.createSpy("confirmResolve");
        confirmReject = jasmine.createSpy("confirmReject");
    });

    afterEach(function() {
        confirmResolve.calls.reset();
        confirmReject.calls.reset();
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe("Function: constructor", () => {
        it("should create an odata changeset", () => {
            let changeset:odata.IODataChangeset = new odata.OData3Changeset(uuid);
            expect(changeset instanceof odata.OData3Changeset).toEqual(true);

            let expected = "Content-Type: multipart/mixed; boundary=changeset_12345670-1000-a000-89ab-000000000000\r\n"
                + "Content-Transfer-Encoding:binary\r\n"
                + "\r\n"
                + "--changeset_12345670-1000-a000-89ab-000000000000--\r\n"

            expect(changeset.Id).toEqual("changeset_12345670-1000-a000-89ab-000000000000");
            expect(changeset.Body).toEqual(expected);
        });
    });

    describe("Function: addRequest", () => {
        it("should create an odata request without a content id", () => {
            let changeset:odata.IODataChangeset = new odata.OData3Changeset(uuid);
            expect(changeset instanceof odata.OData3Changeset).toEqual(true);
            let request = service.createRequest("GET", "RequestUrl",{"foo":"bar"}, null);
            changeset.addRequest(request);
           
            let expected = "Content-Type: multipart/mixed; boundary=changeset_12345670-1000-a000-89ab-000000000000\r\n"
                + "Content-Transfer-Encoding:binary\r\n"
                + "\r\n"
                + "--changeset_12345670-1000-a000-89ab-000000000000\r\n"
                + "Content-Type: application/http\r\n"
                + "Content-Transfer-Encoding:binary\r\n"
                + "\r\n"
                + "GET RequestUrl HTTP/1.1\r\n"
                + "DataServiceVersion: 3.0;NetFx\r\n"
                + "MaxDataServiceVersion: 3.0;NetFx\r\n"
                + "Content-Type: application/json;odata=minimalmetadata\r\n"
                + "Accept: application/json;odata=minimalmetadata\r\n"
                + "Accept-Charset: UTF-8\r\n"
                + "\r\n\r\n"
                + "{\"foo\":\"bar\"}"
                + "\r\n\r\n"
                + "--changeset_12345670-1000-a000-89ab-000000000000--\r\n";
            expect(changeset.Body).toEqual(expected);
        });

        it("should create an odata request with a content id", () => {
            let changeset:odata.IODataChangeset = new odata.OData3Changeset(uuid);
            expect(changeset instanceof odata.OData3Changeset).toEqual(true);
            let request = service.createRequest("GET", "RequestUrl",{"foo":"bar"}, "1");
            changeset.addRequest(request);
           
            let expected = "Content-Type: multipart/mixed; boundary=changeset_12345670-1000-a000-89ab-000000000000\r\n"
                + "Content-Transfer-Encoding:binary\r\n"
                + "\r\n"
                + "--changeset_12345670-1000-a000-89ab-000000000000\r\n"
                + "Content-Type: application/http\r\n"
                + "Content-Transfer-Encoding:binary\r\n"
                + "\r\n"
                + "GET RequestUrl HTTP/1.1\r\n"
                + "DataServiceVersion: 3.0;NetFx\r\n"
                + "MaxDataServiceVersion: 3.0;NetFx\r\n"
                + "Content-Type: application/json;odata=minimalmetadata\r\n"
                + "Accept: application/json;odata=minimalmetadata\r\n"
                + "Accept-Charset: UTF-8\r\n"
                + "Content-ID:1\r\n"
                + "\r\n"
                + "{\"foo\":\"bar\"}"
                + "\r\n\r\n"
                + "--changeset_12345670-1000-a000-89ab-000000000000--\r\n";
            expect(changeset.Body).toEqual(expected);
        });
    });
});