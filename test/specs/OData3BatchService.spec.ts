import * as odata from "./../../src/angular-odata";
import { MockUUID } from "./../mocks/MockUUID";
import * as angular from 'angular';

angular.module('AngularODataTestModule', [])
    .service("odata3BatchService",odata.OData3BatchService)
    .service("uuid",MockUUID);

describe("Service: OData3BatchService", () => {
    var service:odata.IODataBatchService;
    var $q:ng.IQService;
    var $scope:ng.IScope;
    var $httpBackend:ng.IHttpBackendService;
    var $httpParamSerializer:ng.IHttpParamSerializer;
    var $http:ng.IHttpService;
    var getRequestHandler:angular.mock.IRequestHandler;
    var confirmResolve:jasmine.Spy;
    var confirmReject:jasmine.Spy;

    beforeEach(function(){
        angular.mock.module('AngularODataTestModule');

        inject(function($rootScope, _$q_, _$httpBackend_:ng.IHttpBackendService, _$httpParamSerializer_:ng.IHttpParamSerializer,
            odata3BatchService:odata.IODataBatchService){
            service = odata3BatchService;
            $q = _$q_;
            $scope = $rootScope.$new();
            $httpBackend = _$httpBackend_;
            $httpParamSerializer = _$httpParamSerializer_;
        });

        confirmResolve = jasmine.createSpy("confirmResolve");
        confirmReject = jasmine.createSpy("confirmReject");
    });

    afterEach(function() {
        confirmResolve.calls.reset();
        confirmReject.calls.reset();
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it("Service should exist", () => {
        expect(service).toBeDefined();
    });

    describe("Function: createRequest", () => {
        it("Service should implement createRequest", () => {
            expect(service.createRequest).toBeDefined();
        });

        it("should create an odata request without a content id", () => {
            let request = service.createRequest("GET", "RequestUrl",{"foo":"bar"}, null);
            expect(request instanceof odata.OData3Request).toEqual(true);
            let expected = "Content-Type: application/http\r\n"
                + "Content-Transfer-Encoding:binary\r\n"
                + "\r\n"
                + "GET RequestUrl HTTP/1.1\r\n"
                + "DataServiceVersion: 3.0;NetFx\r\n"
                + "MaxDataServiceVersion: 3.0;NetFx\r\n"
                + "Content-Type: application/json;odata=minimalmetadata\r\n"
                + "Accept: application/json;odata=minimalmetadata\r\n"
                + "Accept-Charset: UTF-8\r\n"
                + "\r\n\r\n"
                + "{\"foo\":\"bar\"}"
                + "\r\n"

            expect(request.Body).toEqual(expected);
        });

        it("should create an odata request with a content id", () => {
            let request = service.createRequest("GET", "RequestUrl",{"foo":"bar"},"1");
            expect(request instanceof odata.OData3Request).toEqual(true);
            let expected = "Content-Type: application/http\r\n"
                + "Content-Transfer-Encoding:binary\r\n"
                + "\r\n"
                + "GET RequestUrl HTTP/1.1\r\n"
                + "DataServiceVersion: 3.0;NetFx\r\n"
                + "MaxDataServiceVersion: 3.0;NetFx\r\n"
                + "Content-Type: application/json;odata=minimalmetadata\r\n"
                + "Accept: application/json;odata=minimalmetadata\r\n"
                + "Accept-Charset: UTF-8\r\n"
                + "Content-ID:1\r\n\r\n"
                + "{\"foo\":\"bar\"}"
                + "\r\n"
            expect(request.Body).toEqual(expected);
        });
    });
    
    describe("Function: createChangeset", () => {
        it("Service should implement createChangeset", () => {
            expect(service.createChangeset).toBeDefined();
        });

        it("should create a changeset", () => {
            let changeset:odata.IODataChangeset = service.createChangeset();
            let expected = "Content-Type: multipart/mixed; boundary=changeset_12345670-1000-a000-89ab-000000000000\r\n"
                + "Content-Transfer-Encoding:binary\r\n"
                + "\r\n"
                + "--changeset_12345670-1000-a000-89ab-000000000000--\r\n"

            expect(changeset.Id).toEqual("changeset_12345670-1000-a000-89ab-000000000000");
            expect(changeset.Body).toEqual(expected);
        });
    });

    describe("Function: createBatch", () => {
        it("Service should implement createBatch", () => {
            expect(service.createBatch).toBeDefined();
        });

        it("should create an odata batch", () => {
            let batch:odata.IODataBatch = service.createBatch("BatchUrl");
            expect(batch instanceof odata.OData3Batch).toEqual(true);
            expect(batch.Url).toEqual("BatchUrl");
            expect(batch.Id).toEqual("batch_12345670-1000-a000-89ab-000000000000");
            expect(batch.Headers).toEqual(jasmine.objectContaining({
                    "Accept": "multipart/mixed",
                    "Content-Type": "multipart/mixed;boundary=" + batch.Id,
                    "DataServiceVersion": "3.0;NetFx",
                    "MaxDataServiceVersion": "3.0;NetFx"
                }));
            expect(batch.Body).toEqual("--batch_12345670-1000-a000-89ab-000000000000\r\n--batch_12345670-1000-a000-89ab-000000000000--\r\n")
        });
    });

    describe("Function: submitBatch", () => {
        var requestHandler:angular.mock.IRequestHandler;

        describe("Response Handling", () => {
            it("should handle a successful batch response", () => {
                var requestHandler = $httpBackend.when("POST","$batch")
                    .respond((method,url,data,headers,params) => {
                        let resultData = {"odata.metadata":"https://aoc-api-edr-dev.courts.wa.gov/DataServices/AocEdrDataService.svc/$metadata#DataSources/@Element","Key":"918","CreateTimeStamp":"2017-02-09T11:00:11.109449","CreateCredential":"AOC","UpdateTimeStamp":"2017-02-09T11:00:11.109449","UpdateCredential":"AOC","CodeDescription":"Data source for EDR Dev","BeginDate":null,"EndDate":null};
                        
                        let response = "--batchresponse_da3621a8-2e2b-4780-aa13-21eb29b8d3fa\r\n"
                            + "Content-Type: application/http\r\n"
                            + "Content-Transfer-Encoding: binary\r\n"
                            + "\r\n"
                            + "HTTP/1.1 200 OK\r\n"
                            + "DataServiceVersion: 3.0;\r\n"
                            + "Content-Type: application/json;odata=minimalmetadata;streaming=true;charset=utf-8\r\n"
                            + "X-Content-Type-Options: nosniff\r\n"
                            + "Cache-Control: no-cache\r\n"
                            + "\r\n"
                            + "{\"foo\":\"bar\"}\r\n"
                            + "--batchresponse_da3621a8-2e2b-4780-aa13-21eb29b8d3fa--\r\n"
                        
                        return [202,response,{"Content-Type":"multipart/mixed; boundary=batchresponse_da3621a8-2e2b-4780-aa13-21eb29b8d3fa"}];
                });
                let batch:odata.IODataBatch = service.createBatch("$batch");
                let request = service.createRequest("GET", "RequestUrl",{"foo":"bar"},null);

                service.submitBatch(batch).then(responses => {
                    confirmResolve();
                    expect(responses.length).toEqual(1);
                    let response = responses[0];
                    expect(response.ResponseCode).toEqual("200");
                    expect(response.Success).toBeTruthy();
                    expect(response.Headers.length).toEqual(4);
                    expect(response.Data).toEqual('{"foo":"bar"}');
                }).catch(reject => {
                    confirmReject()
                });

                $httpBackend.flush();
                expect(confirmResolve).toHaveBeenCalled();
                expect(confirmReject).not.toHaveBeenCalled();
            });

            it("should handle a successful batch response with unsuccessful items", () => {
                var resultData = {"odata.error":{"code":"","message":{"lang":"en-US","value":"{\"ErrorCode\":404,\"ComponentErrors\":[{\"ComponentDescription\":\"Data Service Framework\",\"ErrorDetails\":[{\"ErrorCode\":404,\"ErrorDescription\":\"DataServiceException\",\"ErrorDetails\":[\"Resource not found for the segment 'DataSources'.\"]}]}]}"}}};
                        
                var requestHandler = $httpBackend.when("POST","$batch")
                    .respond((method,url,data,headers,params) => {
                        
                        let response = "--batchresponse_da3621a8-2e2b-4780-aa13-21eb29b8d3fa\r\n"
                            + "Content-Type: application/http\r\n"
                            + "Content-Transfer-Encoding: binary\r\n"
                            + "\r\n"
                            + "HTTP/1.1 404 Not Found\r\n"
                            + "DataServiceVersion: 3.0;\r\n"
                            + "Content-Type: application/json;odata=minimalmetadata;streaming=true;charset=utf-8\r\n"
                            + "X-Content-Type-Options: nosniff\r\n"
                            + "Cache-Control: no-cache\r\n"
                            + "\r\n"
                            + JSON.stringify(resultData) + "\r\n"
                            + "--batchresponse_da3621a8-2e2b-4780-aa13-21eb29b8d3fa--\r\n"
                        
                        return [202,response,{"Content-Type":"multipart/mixed; boundary=batchresponse_da3621a8-2e2b-4780-aa13-21eb29b8d3fa"}];
                });
                let batch:odata.IODataBatch = service.createBatch("$batch");
                let request = service.createRequest("GET", "RequestUrl",{"foo":"bar"},null);

                service.submitBatch(batch).then(responses => {
                    confirmResolve();
                    expect(responses.length).toEqual(1);
                    let response = responses[0];
                    expect(response.ResponseCode).toEqual("404");
                    expect(response.Success).toBeFalsy();
                    expect(response.Headers.length).toEqual(4);
                    expect(response.Data).toEqual(JSON.stringify(resultData));                
                }).catch(reject => {
                    confirmReject()
                });

                $httpBackend.flush();
                expect(confirmResolve).toHaveBeenCalled();
                expect(confirmReject).not.toHaveBeenCalled();
            });

            it("should handle a successful batch response with a bad content-type boundary header", () => {
                var resultData = {"odata.error":{"code":"","message":{"lang":"en-US","value":"{\"ErrorCode\":404,\"ComponentErrors\":[{\"ComponentDescription\":\"Data Service Framework\",\"ErrorDetails\":[{\"ErrorCode\":404,\"ErrorDescription\":\"DataServiceException\",\"ErrorDetails\":[\"Resource not found for the segment 'DataSources'.\"]}]}]}"}}};
                        
                var requestHandler = $httpBackend.when("POST","$batch")
                    .respond((method,url,data,headers,params) => {
                        
                        let response = "--batchresponse_da3621a8-2e2b-4780-aa13-21eb29b8d3fa\r\n"
                            + "Content-Type: application/http\r\n"
                            + "Content-Transfer-Encoding: binary\r\n"
                            + "\r\n"
                            + "HTTP/1.1 404 Not Found\r\n"
                            + "DataServiceVersion: 3.0;\r\n"
                            + "Content-Type: application/json;odata=minimalmetadata;streaming=true;charset=utf-8\r\n"
                            + "X-Content-Type-Options: nosniff\r\n"
                            + "Cache-Control: no-cache\r\n"
                            + "\r\n"
                            + JSON.stringify(resultData) + "\r\n"
                            + "--batchresponse_da3621a8-2e2b-4780-aa13-21eb29b8d3fa--\r\n"
                        
                        return [202,response,{"Content-Type":"multipart/mixed;"}];
                });
                let batch:odata.IODataBatch = service.createBatch("$batch");
                let request = service.createRequest("GET", "RequestUrl",{"foo":"bar"},null);

                service.submitBatch(batch).then(responses => {
                    confirmResolve();              
                }).catch(reject => {
                    confirmReject();
                    expect(reject).toEqual("Bad content-type header, no multipart boundary")
                });

                $httpBackend.flush();
                expect(confirmResolve).not.toHaveBeenCalled();
                expect(confirmReject).toHaveBeenCalled();
            });

            it("should handle an unsuccessful response", () => {
                var requestHandler = $httpBackend.when("POST","$batch")
                    .respond((method,url,data,headers,params) => {
                        return [500,"Internal Server Error"];
                });
                let batch:odata.IODataBatch = service.createBatch("$batch");
                let request = service.createRequest("GET", "RequestUrl",{"foo":"bar"},null);

                service.submitBatch(batch).then(responses => {
                    confirmResolve();             
                }).catch(reject => {
                    confirmReject()
                    expect(reject.status).toEqual(500);
                    expect(reject.data).toEqual("Internal Server Error");
                });

                $httpBackend.flush();
                expect(confirmResolve).not.toHaveBeenCalled();
                expect(confirmReject).toHaveBeenCalled();
            });

            it("should handle a successful response with unparseable response", () => {
                var resultData = {"odata.error":{"code":"","message":{"lang":"en-US","value":"{\"ErrorCode\":404,\"ComponentErrors\":[{\"ComponentDescription\":\"Data Service Framework\",\"ErrorDetails\":[{\"ErrorCode\":404,\"ErrorDescription\":\"DataServiceException\",\"ErrorDetails\":[\"Resource not found for the segment 'DataSources'.\"]}]}]}"}}};
                        
                var requestHandler = $httpBackend.when("POST","$batch")
                    .respond((method,url,data,headers,params) => {
                        
                        let response = "ThisIsn'tAGoodResponse";
                        
                        return [202,response,{"Content-Type":"multipart/mixed; boundary=batchresponse_da3621a8-2e2b-4780-aa13-21eb29b8d3fa"}];
                });
                let batch:odata.IODataBatch = service.createBatch("$batch");
                let request = service.createRequest("GET", "RequestUrl",{"foo":"bar"},null);

                service.submitBatch(batch).then(responses => {
                    confirmResolve();
                    expect(responses.length).toEqual(0);            
                }).catch(reject => {
                    confirmReject()
                });

                $httpBackend.flush();
                expect(confirmResolve).toHaveBeenCalled();
                expect(confirmReject).not.toHaveBeenCalled();
            });

            it("should handle a successful response with a changeset", () => {
                var requestHandler = $httpBackend.when("POST","$batch")
                    .respond((method,url,data,headers,params) => {
                        let response = "--batchresponse_e19cb240-b355-403e-a11c-4b930379dea5\r\n"
                            + "Content-Type: multipart/mixed; boundary=changesetresponse_00e33c9c-19d2-4edb-9a19-d731bd2746cd\r\n"
                            + "\r\n"
                            + "--changesetresponse_00e33c9c-19d2-4edb-9a19-d731bd2746cd\r\n"
                            + "Content-Type: application/http\r\n"
                            + "Content-Transfer-Encoding: binary\r\n"
                            + "\r\n"
                            + "HTTP/1.1 201 Created\r\n"
                            + "DataServiceVersion: 3.0;\r\n"
                            + "Content-Type: application/json;odata=minimalmetadata;streaming=true;charset=utf-8\r\n"
                            + "Content-ID: 1\r\n"
                            + "X-Content-Type-Options: nosniff\r\n"
                            + "Cache-Control: no-cache\r\n"
                            + "Location: https://aoc-api-edr-dev.courts.wa.gov/DataServices/AocEdrDataService.svc/SecurityCertificates('7f83a9a0f3a411e6a374d7346a129938')\r\n"
                            + "\r\n"
                            + "{'TestResult':'TestResultData'}\r\n"
                            + "--changesetresponse_00e33c9c-19d2-4edb-9a19-d731bd2746cd\r\n"
                            + "Content-Type: application/http\r\n"
                            + "Content-Transfer-Encoding: binary\r\n"
                            + "\r\n"
                            + "HTTP/1.1 204 No Content\r\n"
                            + "X-Content-Type-Options: nosniff\r\n"
                            + "Cache-Control: no-cache\r\n"
                            + "DataServiceVersion: 3.0;\r\n"
                            + "\r\n"
                            + "\r\n"
                            + "--changesetresponse_00e33c9c-19d2-4edb-9a19-d731bd2746cd--\r\n"
                            + "--batchresponse_e19cb240-b355-403e-a11c-4b930379dea5--\r\n"

                        return [202,response,{"Content-Type":"multipart/mixed; boundary=batchresponse_e19cb240-b355-403e-a11c-4b930379dea5"}];
                });
                let batch:odata.IODataBatch = service.createBatch("$batch");
                let request = service.createRequest("GET", "RequestUrl",{"foo":"bar"},null);

                service.submitBatch(batch).then(responses => {
                    confirmResolve();
                    expect(responses.length).toEqual(2);
                    let response = responses[0];
                    expect(response.ResponseCode).toEqual("201");
                    expect(response.ResponseText).toEqual("Created");
                    expect(response.Success).toBeTruthy();
                    expect(response.Headers.length).toEqual(6);
                    expect(response.Data).toEqual("{'TestResult':'TestResultData'}");

                    response = responses[1];
                    expect(response.ResponseCode).toEqual("204");
                    expect(response.ResponseText).toEqual("No Content");
                    expect(response.Success).toBeTruthy();
                    expect(response.Headers.length).toEqual(3);
                    expect(response.Data).toEqual("");
                }).catch(reject => {
                    confirmReject()
                });

                $httpBackend.flush();
                expect(confirmResolve).toHaveBeenCalled();
                expect(confirmReject).not.toHaveBeenCalled();
            });
        });

    });
});