import * as odata from "./../../src/angular-odata";
import { MockUUID } from "./../mocks/MockUUID";

import * as angular from 'angular';

angular.module('AngularODataTestModule', [])
    .service("odata3BatchService",odata.OData3BatchService)
    .service("uuid",MockUUID);

describe("Service: OData3BatchService", () => {
    var service:odata.IODataBatchService;
    var $q:ng.IQService;
    var uuid:MockUUID;
    var $scope:ng.IScope;
    var $httpBackend:ng.IHttpBackendService;
    var $httpParamSerializer:ng.IHttpParamSerializer;
    var $http:ng.IHttpService;
    var getRequestHandler:angular.mock.IRequestHandler;
    var confirmResolve:jasmine.Spy;
    var confirmReject:jasmine.Spy;

    beforeEach(function(){
        angular.mock.module('AngularODataTestModule');

        inject(function($rootScope, _$q_, _$httpBackend_:ng.IHttpBackendService, _$httpParamSerializer_:ng.IHttpParamSerializer,
            odata3BatchService:odata.IODataBatchService, _uuid_:MockUUID){
            service = odata3BatchService;
            $q = _$q_;
            $scope = $rootScope.$new();
            $httpBackend = _$httpBackend_;
            $httpParamSerializer = _$httpParamSerializer_;
            uuid = _uuid_;
        });

        confirmResolve = jasmine.createSpy("confirmResolve");
        confirmReject = jasmine.createSpy("confirmReject");
    });

    afterEach(function() {
        confirmResolve.calls.reset();
        confirmReject.calls.reset();
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe("Function: constructor", () => {
        it("should create an odata batch", () => {
            let batch:odata.IODataBatch = new odata.OData3Batch("BatchUrl",uuid);
            expect(batch instanceof odata.OData3Batch).toEqual(true);
            expect(batch.Url).toEqual("BatchUrl");
            expect(batch.Id).toEqual("batch_12345670-1000-a000-89ab-000000000000");
            expect(batch.Headers).toEqual(jasmine.objectContaining({
                    "Accept": "multipart/mixed",
                    "Content-Type": "multipart/mixed;boundary=" + batch.Id,
                    "DataServiceVersion": "3.0;NetFx",
                    "MaxDataServiceVersion": "3.0;NetFx"
                }));
            expect(batch.Body).toEqual("--batch_12345670-1000-a000-89ab-000000000000\r\n--batch_12345670-1000-a000-89ab-000000000000--\r\n")
        });
    });

    describe("Function: createChangeset", () => {
        it("Service should implement createChangeset", () => {
            expect(service.createChangeset).toBeDefined();
        });

        it("should create a changeset", () => {
            let batch:odata.IODataBatch = new odata.OData3Batch("BatchUrl",uuid);
            let changeset:odata.IODataChangeset = service.createChangeset();
            batch.addChangeset(changeset);
           
            
            let expected = "--batch_12345670-1000-a000-89ab-000000000000\r\n"
                + "Content-Type: multipart/mixed; boundary=changeset_12345670-1000-a000-89ab-000000000001\r\n"
                + "Content-Transfer-Encoding:binary\r\n"
                + "\r\n"
                + "--changeset_12345670-1000-a000-89ab-000000000001--\r\n"
                + "\r\n"
                + "--batch_12345670-1000-a000-89ab-000000000000--\r\n"
          expect(batch.Body).toEqual(expected);
          expect(batch.Queue.length).toEqual(1);
        });
    });
    

    describe("Function: addRequest", () => {
        it("should create an odata request without a content id", () => {
            let batch:odata.IODataBatch = new odata.OData3Batch("BatchUrl",uuid);
            let request = service.createRequest("GET", "RequestUrl",{"foo":"bar"}, null);
            batch.addRequest(request);
            
            let expected = "--batch_12345670-1000-a000-89ab-000000000000\r\n"
                + "Content-Type: application/http\r\n"
                + "Content-Transfer-Encoding:binary\r\n"
                + "\r\n"
                + "GET RequestUrl HTTP/1.1\r\n"
                + "DataServiceVersion: 3.0;NetFx\r\n"
                + "MaxDataServiceVersion: 3.0;NetFx\r\n"
                + "Content-Type: application/json;odata=minimalmetadata\r\n"
                + "Accept: application/json;odata=minimalmetadata\r\n"
                + "Accept-Charset: UTF-8\r\n"
                + "\r\n\r\n"
                + "{\"foo\":\"bar\"}"
                + "\r\n\r\n"
                + "--batch_12345670-1000-a000-89ab-000000000000--\r\n"

            expect(batch.Body).toEqual(expected);
        });
    });

    describe("Function: getBatchRequest", () => {
        it("should implement getBatchRequest", () => {
            let batch:odata.IODataBatch = new odata.OData3Batch("BatchUrl",uuid);
            expect(batch.getBatchRequest).toBeDefined();
        });

        it("should return odata batch data", () => {
            let batch:odata.IODataBatch = new odata.OData3Batch("BatchUrl",uuid);
            let batchRequest = batch.getBatchRequest();
            expect(batchRequest.url).toEqual("BatchUrl");
            expect(batchRequest.headers).toEqual(jasmine.objectContaining({
                    "Accept": "multipart/mixed",
                    "Content-Type": "multipart/mixed;boundary=" + batch.Id,
                    "DataServiceVersion": "3.0;NetFx",
                    "MaxDataServiceVersion": "3.0;NetFx"
                }));
            expect(batchRequest.body).toEqual("--batch_12345670-1000-a000-89ab-000000000000\r\n--batch_12345670-1000-a000-89ab-000000000000--\r\n")
        });
    });
});