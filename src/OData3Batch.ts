import { IODataBatch } from './IODataBatch';
import { IODataRequest } from "./IODataRequest"
import { IODataChangeset } from "./IODataChangeset"

export class OData3Batch implements IODataBatch {
    private _batchId: string;
    private _batchUrl: string;
    private _batchQueue:(IODataRequest|IODataChangeset)[]=[];

    get Id():string {
        return this._batchId;
    }

    get Url():string {
        return this._batchUrl;
    }

    get Headers():{} {
         return {
            "Accept":"multipart/mixed",
            "Content-Type":"multipart/mixed;boundary="+this.Id,
            "DataServiceVersion":"3.0;NetFx",
            "MaxDataServiceVersion":"3.0;NetFx"
        }
    }

    get Body():string {
        let bodyHeader = '--' + this.Id + "\r\n";
        let bodyFooter = '--' + this.Id + "--\r\n";
        let body = "";
        this.Queue.forEach(item => {
            body += item.Body + "\r\n";
        })
        return bodyHeader + body + bodyFooter;
    }

    get Queue():(IODataRequest|IODataChangeset)[] {
        return this._batchQueue;
    }

    constructor(url:string,private uuid:any) {
        this._batchId = "batch_" + uuid.v1();
        this._batchUrl = url;
    }

    public addChangeset(changeset:IODataChangeset):void {
        this._batchQueue.push(changeset);
    }
    public addRequest(request:IODataRequest):void{
        this._batchQueue.push(request);
    };

    public getBatchRequest():{url:string,headers:{},body:string} {
        return {
            url: this.Url,
            headers:this.Headers,
            body: this.Body,
        }
    }
}