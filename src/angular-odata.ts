import { OData3BatchService } from "./OData3BatchService";
import * as angular from "angular";

angular.module('angular-odata',['angular-uuid'])
    .service('odata3BatchService', OData3BatchService);

export * from "./IODataBatch";
export * from "./IODataBatchService";
export * from "./IODataChangeset";
export * from "./IODataRequest";
export * from "./IODataResponse";
export * from "./OData3Batch";
export * from "./OData3BatchService";
export * from "./OData3Changeset";
export * from "./OData3Request";
export * from "./OData3Response";
