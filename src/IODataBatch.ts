import { IODataRequest } from "./IODataRequest"
import { IODataChangeset } from "./IODataChangeset"

export interface IODataBatch {
    readonly Id:string;
    readonly Url:string;
    readonly Body:string;
    readonly Headers:{};
    readonly Queue:(IODataChangeset|IODataRequest)[];

    addChangeset(changeset:IODataChangeset):void;
    addRequest(request:IODataRequest):void;
    getBatchRequest():{url:string,headers:{},body:string};
}


