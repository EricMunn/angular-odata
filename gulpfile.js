var gulp = require('gulp');
var clean = require('gulp-clean');

gulp.task('clean-dev', function() {
    return gulp.src([
        './src/**/*.js',
        './src/**/*.js.map',
        './test/**/*.js',
        './test/**/*.js.map',
        ], {read:false})
        .pipe(clean());
});

gulp.task('default', ['clean-dev']);